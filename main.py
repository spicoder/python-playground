from fastapi import FastAPI

app = FastAPI()


# Define a GET endpoint
@app.get("/")
async def read_root():
    return {"message": "Hello pi"}


# Define a POST endpoint
@app.post("/items/")
async def create_item(item: dict):
    return {"item": item}
